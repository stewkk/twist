#include <wheels/test/framework.hpp>

#include <twist/run/cross.hpp>

#include <twist/ed/std/thread.hpp>

#include <twist/ed/spin/wait.hpp>
#include <twist/ed/spin/lock.hpp>

#include <wheels/core/compiler.hpp>

using twist::ed::std::thread;

using twist::ed::SpinWait;
using twist::ed::SpinLock;

namespace this_thread = twist::ed::std::this_thread;

using namespace std::chrono_literals;


TEST_SUITE(SpinWait) {
  SIMPLE_TEST(JustWorks) {
    twist::run::Cross([] {
      SpinWait spin_wait;

      // operator()
      spin_wait();

      spin_wait.Spin();
    });
  }

  SIMPLE_TEST(Spin1234) {
    twist::run::Cross([] {
      SpinWait spin_wait;

      for (size_t i = 0; i < 1234; ++i) {
        spin_wait();
      }
    });
  }
}

TEST_SUITE(SpinLock) {
  SIMPLE_TEST(LockUnlock) {
    twist::run::Cross([] {
      SpinLock spinlock;

      spinlock.Lock();
      spinlock.Unlock();
    });
  }

  SIMPLE_TEST(RAII) {
    twist::run::Cross([] {
      SpinLock spinlock;

      {
        std::lock_guard guard{spinlock};
      }

      {
        std::unique_lock lock{spinlock};
        bool owns = lock.owns_lock();
        WHEELS_UNUSED(owns);
      }
    });
  }

  SIMPLE_TEST(TryLock) {
    twist::run::Cross([] {
      SpinLock spinlock;

      {
        // Success

        while (!spinlock.TryLock()) {
          // Spurious failure, try again
        }
        spinlock.Unlock();
      }

      {
        // Failure

        spinlock.Lock();

        for (size_t i = 0; i < 100; ++i) {
          ASSERT_FALSE(spinlock.TryLock());
        }

        spinlock.Unlock();
      }

      {
        // RAII
        std::unique_lock lock{spinlock, std::try_to_lock};
      }
    });
  }

  SIMPLE_TEST(Wait) {
    twist::run::Cross([] {
      SpinLock spinlock;

      bool t_cs = false;

      spinlock.Lock();

      thread t([&] {
        spinlock.Lock();
        t_cs = true;
        spinlock.Unlock();
      });

      this_thread::sleep_for(1s);
      ASSERT_FALSE(t_cs);

      spinlock.Unlock();

      t.join();
      ASSERT_TRUE(t_cs);
    });
  }
}
