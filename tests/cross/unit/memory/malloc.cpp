#include <wheels/test/framework.hpp>

#include <twist/run/cross.hpp>

#include <twist/ed/mem/malloc.hpp>

TEST_SUITE(Malloc) {
  void Write(void* ptr, size_t size) {
    char* buf = (char*)ptr;
    buf[0] = 0x1;
    buf[size - 1] = 0x1;
  }

  SIMPLE_TEST(JustWorks) {
    twist::run::Cross([] {
      void* p = twist::ed::mem::Malloc(17);
      Write(p, 17);
      twist::ed::mem::Free(p);
    });
  }

  SIMPLE_TEST(SmallSizes) {
    twist::run::Cross([] {
      for (size_t s = 1; s <= 10; ++s) {
        void* p = twist::ed::mem::Malloc(s);
        Write(p, s);
        twist::ed::mem::Free(p);
      }
    });
  }

  SIMPLE_TEST(PowerOf2Sizes) {
    twist::run::Cross([] {
      for (size_t i = 0; i < 12; ++i) {
        void* p = twist::ed::mem::Malloc(1 << i);
        Write(p, 1 << i);
        twist::ed::mem::Free(p);
      }
    });
  }
}
