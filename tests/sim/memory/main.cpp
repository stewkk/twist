#include <twist/run/sim.hpp>

#include <cassert>
#include <random>

using namespace std::chrono_literals;

static_assert(twist::build::IsolatedSim());

int main() {
  {
    // delete nullptr

    auto params = twist::run::sim::Params{};
    params.sim.memset_malloc = 0xFF;

    twist::run::Sim(params, [] {
      int* p = nullptr;
      delete p;
    });
  }

  {
    // Free list resize

    struct Node {
      Node* next;
    };

    twist::run::Sim([] {
      Node* top = nullptr;
      for (size_t i = 0; i < 4096; ++i) {
        Node* new_node = new Node{top};
        top = new_node;
      }

      while (top != nullptr) {
        Node* next = top->next;
        delete top;
        top = next;
      }
    });
  }

  {
    // Fill memory

    auto params = twist::run::sim::Params{};
    params.sim.memset_malloc = 0xFF;

    twist::run::Sim(params, [] {
      int* p = new int;
      assert(*p != 0);
      delete p;
    });
  }

  {
    // Randomization

    struct Node {
      // Empty
    };

    auto params = twist::run::sim::Params{};
    params.sim.randomize_malloc = true;

    twist::run::Sim(params, [] {
      static const size_t kNodes = 4;

      std::array<Node*, kNodes> nodes;

      {
        // Prepare free list
        for (size_t i = 0; i < kNodes; ++i) {
          nodes[i] = new Node{};
        }
        for (size_t i = 0; i < kNodes; ++i) {
          delete nodes[i];
        }
      }

      std::random_device d{};

      for (size_t i = 0; i < 64; ++i) {
        // Random permutation
        std::shuffle(nodes.begin(), nodes.end(), d);

        while (true) {
          size_t match = 0;
          for (size_t j = 0; j < kNodes; ++j) {
            Node* n = new Node{};
            if (n == nodes[j]) {
              ++match;
            } else {
              delete n;
              break;
            }
          }

          // Cleanup
          for (size_t j = 0; j < match; ++j) {
            delete nodes[j];
          }

          if (match == kNodes) {
            break;
          }
        }
      }
    });
  }

  {
    // Double-free

    auto params = twist::run::sim::Params{};
    params.sim.crash_on_abort = false;

    auto result = twist::run::Sim(params, [] {
      int* p = new int{17};
      delete p;
      delete p;  // <- Aborted

      WHEELS_UNREACHABLE();
    });

    assert(!result.Ok());
    assert(result.status == twist::run::sim::Status::MemoryDoubleFree);
    fmt::println("Stderr: {}", result.std_err);
  }

  {
    // Memory leak

    auto params = twist::run::sim::Params{};
    params.sim.crash_on_abort = false;

    auto result = twist::run::Sim(params, [] {
      new int{1};
      new char[128];
    });

    assert(!result.Ok());
    assert(result.status == twist::run::sim::Status::MemoryLeak);
    fmt::println("Stderr: {}", result.std_err);
  }

  return 0;
}
