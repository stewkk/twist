#pragma once

namespace twist::build {

constexpr bool Sim() {
#if defined(__TWIST_FIBERS__) && defined(__TWIST_FAULTY__)
  return true;
#else
  return false;
#endif
}

constexpr bool IsolatedSim() {
#if defined(__TWIST_FIBERS__) && defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__) && defined(__TWIST_FAULTY__)
  return true;
#else
  return false;
#endif
}

constexpr bool Threaded() {
#if defined(__TWIST_FIBERS__)
  return false;
#else
  return true;
#endif
}

constexpr bool Faulty() {
#if defined(__TWIST_FAULTY__)
  return true;
#else
  return false;
#endif
}

}  // namespace twist::build
