#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/fair/scheduler.hpp>

namespace twist::sim::sched {

namespace fair {

using twist::rt::fiber::system::scheduler::fair::Scheduler;
using SchedulerParams = twist::rt::fiber::system::scheduler::fair::Params;

}  // namespace fair

}  // namespace twist::sim::sched
