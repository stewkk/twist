#pragma once

#include "scheduler/random.hpp"

namespace twist::run::sim {

// Default scheduler
using Scheduler = random::Scheduler;

}  // namespace twist::run::sim
