#include <twist/run/sim.hpp>

#include <wheels/core/panic.hpp>

#include <fmt/core.h>

namespace twist::run {

#if defined(__TWIST_FIBERS__) && defined(__TWIST_FAULTY__)

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

namespace sim {

Digest DetCheckSim(Params params, MainRoutine main) {
  {
    /*
     * /\ Isolated user memory
     * /\ Same memory mapping for same process
     */
    params.sim.digest_atomic_loads = true;
    // Do not crash in determinism check
    params.sim.crash_on_abort = false;
    params.sim.forward_stdout = false;
  }

  static const size_t kIters = 512;

  Scheduler scheduler{params.sched};

  Simulator simulator{&scheduler, params.sim};

  simulator.Silent(true);
  simulator.Start(main);
  simulator.RunFor(kIters);
  auto ret = simulator.Burn();

  return ret.digest;
}

bool DetCheck(Params params, MainRoutine main) {
  Digest d1 = DetCheckSim(params, main);
  Digest d2 = DetCheckSim(params, main);

  if (d1 != d2) {
    return false;
  }

  Digest d3 = DetCheckSim(params, main);

  if (d3 != d1) {
    return false;
  }

  return true;
}

}  // namespace sim

#else

bool sim::DetCheck(sim::Params, MainRoutine) {
  wheels::Panic("Simulation is not supported in this build: TWIST_FIBERS=ON and TWIST_FAULTY=ON expected");
}

#endif

sim::Result Sim(sim::Params params, MainRoutine main) {
  // fmt::println("Seed for deterministic simulation: {}", schedule.seed);

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  WHEELS_VERIFY(sim::DetCheck(params, main), "Simulation is not deterministic");
#endif

  // Scheduler
  sim::Scheduler scheduler{params.sched};
  sim::Simulator simulator{&scheduler, params.sim};

  auto ret = simulator.Run(std::move(main));

  // fmt::println("Simulation digest: {:x}", ret.digest);
  if (!ret.Ok()) {
    fmt::println("Exit status: {}", static_cast<int>(ret.status));
  }

  if (!ret.Ok() && params.sim.crash_on_abort) {
    wheels::Panic(fmt::format("Non-zero exit status ({}), stderr: {}", static_cast<int>(ret.status), ret.std_err));
  }

  return ret;
}

#else

sim::Status Sim(sim::Params, sim::Schedule, MainRoutine) {
  wheels::Panic("Simulation is not supported in this build: TWIST_FIBERS=ON and TWIST_FAULTY=ON expected");
}

bool DetCheck(sim::Params, MainRoutine) {
  wheels::Panic("Simulation is not supported in this build: TWIST_FIBERS=ON and TWIST_FAULTY=ON expected");
}

#endif

sim::Result Sim(MainRoutine main) {
  return Sim(sim::Params{}, std::move(main));
}

bool sim::DetCheck(MainRoutine main) {
  return sim::DetCheck(sim::Params{}, std::move(main));
}

}  // namespace twist::run
