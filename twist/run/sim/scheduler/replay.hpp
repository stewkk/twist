#pragma once

#include <twist/rt/fiber/scheduler/replay/scheduler.hpp>

namespace twist::run::sim {

namespace replay {

using Scheduler = rt::fiber::system::scheduler::replay::Scheduler;

using Params = Scheduler::Params;
using Schedule = Scheduler::Schedule;

}  // namespace replay

}  // namespace twist::run::sim
