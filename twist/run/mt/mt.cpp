#include <twist/run/mt.hpp>

#include <twist/rt/thread/fault/adversary/adversary.hpp>

#include <twist/rt/thread/logging/logging.hpp>

#include <wheels/core/panic.hpp>
#include <wheels/core/assert.hpp>

#include <fmt/core.h>

#if !defined(__TWIST_FIBERS__)

namespace twist::run {

void MultiThread(MainRoutine main) {
#if defined(__TWIST_FAULTY__)
  rt::thread::fault::Adversary()->Reset();
#endif

  main();

#if defined(__TWIST_FAULTY__)
  rt::thread::fault::Adversary()->PrintReport();
#endif

  rt::thread::log::FlushPendingLogMessages();
}

}  // namespace twist::run

#else

namespace twist::run {

void MultiThread(MainRoutine) {
  wheels::Panic("Mt is not supported in this build: TWIST_FIBERS=ON");
}

}  // namespace twist::run

#endif
