#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/assist/preempt.hpp>

namespace twist::rt::facade::assist {

using rt::fiber::user::assist::PreemptionPoint;

}  // namespace twist::rt::facade::assist

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/assist/preempt.hpp>

namespace twist::rt::facade::assist {

using rt::thread::fault::assist::PreemptionPoint;

}  // namespace twist::rt::facade::assist

#else

#include <twist/rt/thread/assist/preempt.hpp>

namespace twist::rt::facade::assist {

using rt::thread::assist::PreemptionPoint;

}  // namespace twist::rt::facade::assist

#endif
