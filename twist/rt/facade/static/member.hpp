#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/static/var.hpp>

#define TWISTED_STATIC_MEMBER_DECLARE(M, name) \
  static twist::rt::fiber::user::StaticVar<M> name

#define TWISTED_STATIC_MEMBER_DEFINE(T, M, name) \
  twist::rt::fiber::user::StaticVar<M> T::name{#name, /*member=*/true}

#else

#include <twist/rt/thread/static/var.hpp>

#define TWISTED_STATIC_MEMBER_DECLARE(M, name) \
  static twist::rt::thread::StaticVar<M> name

#define TWISTED_STATIC_MEMBER_DEFINE(T, M, name) \
  twist::rt::thread::StaticVar<M> T::name{}

#endif
