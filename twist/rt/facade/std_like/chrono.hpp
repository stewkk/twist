#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/chrono.hpp>

namespace twist::rt::facade::std_like::chrono {

using system_clock = rt::fiber::user::library::std_like::chrono::Clock;  // NOLINT
using steady_clock = rt::fiber::user::library::std_like::chrono::Clock;  // NOLINT
using high_resolution_clock = rt::fiber::user::library::std_like::chrono::Clock;  // NOLINT

}  // namespace twist::rt::facade::std_like::chrono

#else

#include <chrono>

namespace twist::rt::facade::std_like::chrono {

using ::std::chrono::system_clock;
using ::std::chrono::steady_clock;
using ::std::chrono::high_resolution_clock;

}  // namespace twist::rt::facade::std_like::chrono

#endif
