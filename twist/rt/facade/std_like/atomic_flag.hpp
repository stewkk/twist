#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/atomic_flag.hpp>

namespace twist::rt::facade::std_like {

using atomic_flag = rt::fiber::user::library::std_like::AtomicFlag;  // NOLINT

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/atomic_flag.hpp>

namespace twist::rt::facade::std_like {

using atomic_flag = rt::thread::fault::FaultyAtomicFlag;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <atomic>

namespace twist::rt::facade::std_like {

using ::std::atomic_flag;

}  // namespace twist::rt::facade::std_like

#endif
