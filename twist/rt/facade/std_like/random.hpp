#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/random.hpp>

namespace twist::rt::facade::std_like {

using random_device = rt::fiber::user::library::std_like::RandomDevice;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <random>

namespace twist::rt::facade::std_like {

using ::std::random_device;

}  // namespace twist::rt::facade::std_like

#endif
