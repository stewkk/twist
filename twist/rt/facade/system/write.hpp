#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/system/write.hpp>

namespace twist::rt::facade {

namespace system {

using fiber::user::library::system::Write;

}  // namespace system

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/system/write.hpp>

namespace twist::rt::facade {

namespace system {

using thread::Write;

}  // namespace system

}  // namespace twist::rt::facade

#endif
