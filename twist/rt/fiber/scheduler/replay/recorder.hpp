#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <wheels/core/assert.hpp>

#include <map>

namespace twist::rt::fiber {

namespace system::scheduler::replay {

class Recorder final : public IScheduler {
 public:
  using Params = replay::Params;
  using Schedule = replay::Schedule;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    WaitQueue(Recorder* recorder, IWaitQueuePtr impl)
        : recorder_(recorder), impl_(std::move(impl)) {
    }

    bool IsEmpty() const override {
      return impl_->IsEmpty();
    }

    void Push(Fiber* waiter) override {
      impl_->Push(waiter);
    }

    Fiber* Pop() override {
      Fiber* waiter = impl_->Pop();
      recorder_->RecordWakeOne(waiter);
      return waiter;
    }

    Fiber* PopAll() override {
      Fiber* waiter = impl_->PopAll();
      recorder_->RecordWakeAll(waiter);
      return waiter;
    }

    bool Remove(Fiber* fiber) override {
      return impl_->Remove(fiber);
    }

   private:
    Recorder* recorder_;
    IWaitQueuePtr impl_;
  };

 public:
  Recorder(IScheduler* underlying)
      : underlying_(underlying) {
  }

  ~Recorder() {
  }

  bool NextSchedule() override {
    return false;
  }

  void Start(Simulator* simulator) override {
    underlying_->Start(simulator);
  }

  // System

  void Interrupt(Fiber* active) override {
    underlying_->Interrupt(active);
  }

  void Yield(Fiber* active) override {
    underlying_->Yield(active);
  }

  void Wake(Fiber* waiter) override {
    underlying_->Wake(waiter);
  }

  void Spawn(Fiber* fiber) override {
    underlying_->Spawn(fiber);
  }

  void Exit(Fiber* fiber) override {
    underlying_->Exit(fiber);
  }

  // Run queue

  bool IsIdle() const override {
    return underlying_->IsIdle();
  }

  Fiber* PickNext() override {
    Fiber* next = underlying_->PickNext();
    RecordPickNext(next->id);
    return next;
  }

  void Remove(Fiber* fiber) override {
    underlying_->Remove(fiber);
  }

  // Hints

  void LockFree(Fiber* fiber, bool flag) override {
    underlying_->LockFree(fiber, flag);
  }

  void Progress(Fiber* fiber) override {
    underlying_->Progress(fiber);
  }

  void NewIter() override {
    underlying_->NewIter();
  }

  // Futex


  IWaitQueuePtr NewWaitQueue() override {
    auto wq = underlying_->NewWaitQueue();
    return std::make_unique<WaitQueue>(this, std::move(wq));
  }

  // Random

  uint64_t RandomNumber() override {
    uint64_t v = underlying_->RandomNumber();
    RecordRandomNumber(v);
    return v;
  }

  size_t RandomChoice(size_t alts) override {
    size_t index = underlying_->RandomChoice(alts);
    RecordRandomChoice(index);
    return index;
  }

  // Spurious

  bool SpuriousWakeup() override {
    bool wake = underlying_->SpuriousWakeup();
    RecordSpuriousWakeup(wake);
    return wake;
  }

  bool SpuriousTryFailure() override {
    bool fail = underlying_->SpuriousTryFailure();
    RecordSpuriousTryFailure(fail);
    return fail;
  }

  //

  Schedule GetSchedule() const {
    return schedule_;
  }

 private:
  void RecordPickNext(FiberId next) {
    schedule_.push_back(decision::PickNext{next});
  }

  void RecordWakeOne(Fiber* waiter) {
    if (waiter != nullptr) {
      schedule_.push_back(decision::WakeOne{waiter->id});
    } else {
      schedule_.push_back(decision::WakeOne{});
    }
  }

  void RecordWakeAll(Fiber* waiter) {
    if (waiter != nullptr) {
      schedule_.push_back(decision::WakeAll{waiter->id});
    } else {
      schedule_.push_back(decision::WakeAll{});
    }
  }

  void RecordRandomNumber(uint64_t value) {
    schedule_.push_back(decision::RandomNumber{value});
  }

  void RecordRandomChoice(size_t index) {
    schedule_.push_back(decision::RandomChoice{index});
  }

  void RecordSpuriousWakeup(bool wake) {
    schedule_.push_back(decision::SpuriousWakeup{wake});
  }

  void RecordSpuriousTryFailure(bool fail) {
    schedule_.push_back(decision::SpuriousTryFailure{fail});
  }

 private:
  IScheduler* underlying_;
  Schedule schedule_;
};

}  // namespace system::scheduler::replay

}  // namespace twist::rt::fiber
