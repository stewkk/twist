#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>
#include <twist/rt/fiber/system/simulator.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

#include <fmt/core.h>

// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace system::scheduler::dfs {

class Scheduler final : public IScheduler {
 public:
  using Params = dfs::Params;
  using Schedule = dfs::Schedule;

 private:
  class SortedList {
   public:
    bool IsEmpty() const {
      return size_ == 0;
    }

    size_t Size() const {
      return size_;
    }

    void Push(Fiber* f) {
      size_t i = size_++;
      while (i > 0 && list_[i - 1]->id > f->id) {
        list_[i] = list_[i - 1];
        --i;
      }
      list_[i] = f;
    }

    Fiber* Pop(size_t i) {
      Fiber* f = list_[i];
      for (size_t j = i; j + 1 < size_; ++j) {
        list_[j] = list_[j + 1];
      }
      --size_;
      return f;
    }

    Fiber* PopLast() {
      if (size_ == 0) {
        return nullptr;
      }
      return list_[--size_];
    }

    bool Remove(Fiber* f) {
      for (size_t i = 0; i < size_; ++i) {
        if (list_[i]->id == f->id) {
          Pop(i);
          return true;
        }
      }
      return false;
    }

    void Clear() {
      size_ = 0;
    }

   private:
    std::array<Fiber*, kMaxFibers> list_;
    size_t size_ = 0;
  };

 private:
  // Futex queue

  class FifoWaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return queue_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return queue_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return queue_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (fiber->wheels::IntrusiveListNode<Fiber, SchedulerTag>::IsLinked()) {
        queue_.Unlink(fiber);
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> queue_;
  };

  class ForkingWaitQueue : public IWaitQueue {
   public:
    ForkingWaitQueue(Scheduler* scheduler)
        : scheduler_(scheduler) {
    }

    bool IsEmpty() const override {
      return list_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return list_.Push(waiter);
    }

    Fiber* Pop() override {
      return scheduler_->PickWaiter(list_);
    }

    Fiber* PopAll() override {
      return list_.PopLast();  // TODO: inverse
    }

    bool Remove(Fiber* fiber) override {
      return list_.Remove(fiber);
    }

   private:
    Scheduler* scheduler_;
    SortedList list_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params) {
  }

  bool NextSchedule() override {
    return Backtrack(schedule_);
  }

  void Start(Simulator* simulator) override {
    simulator_ = simulator;

    fork_index_ = 0;
    preempts_ = 0;
    steps_ = 0;

    run_queue_.Clear();
    active_ = nullptr;
  }

  // System

  void Interrupt(Fiber* active) override {
    active_ = active;
  }

  void Yield(Fiber* active) override {
    run_queue_.Push(active);
  }

  void Wake(Fiber* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.Push(fiber);
  }

  void Exit(Fiber* fiber) override {
    AddStep(fiber);
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    // (active_ != nullptr) => !run_queue_.IsEmpty()
    // WHEELS_ASSERT(!run_queue_.IsEmpty() || (active_ == nullptr), "Broken scheduler");
    return run_queue_.IsEmpty() && (active_ == nullptr);
  }

  Fiber* PickNext() override {
    return PickNext(std::exchange(active_, nullptr));
  }

  void Remove(Fiber* fiber) override {
    run_queue_.Remove(fiber);
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    if (params_.wake_order) {
      return std::make_unique<ForkingWaitQueue>(this);
    } else {
      return std::make_unique<FifoWaitQueue>();
    }
  }

  // Random

  uint64_t RandomNumber() override {
    WHEELS_PANIC("RandomNumber not supported");
  }

  size_t RandomChoice(size_t alts) override {
    return Choice(alts, ForkType::RandomChoice);
  }

  // Spurious

  bool SpuriousWakeup() override {
    if (params_.spurious_wakeups) {
      return Either(ForkType::SpuriousWakeup);
    } else {
      return false;
    }
  }

  bool SpuriousTryFailure() override {
    if (params_.spurious_failures) {
      return Either(ForkType::SpuriousTryFailure);
    } else {
      return false;
    }
  }

 private:
  Fiber* PickNext(Fiber* active) {
    AddStep(active);

    Fiber* preempted = nullptr;
    if (active) {
      if (Preempt(active)) {
        preempted = active;
      } else {
        return active;
      }
    }

    Fiber* next = Pick(run_queue_, ForkType::PickNext);
    if (preempted != nullptr) {
      ++preempts_;
      run_queue_.Push(preempted);
    }
    ResetSteps(next);
    return next;
  }

  bool Preempt(Fiber* active) {
    if (!active->preemptive) {
      return false;
    }

    if (run_queue_.IsEmpty()) {
      return false;  // Alone
    }

    if (ForcedPreempt(active)) {
      if (MaxPreemptsReached()) {
        Prune(::fmt::format(
            "Simulation blocked: force_preempt_after = {} blocked by max_preempts = {}",
            *params_.force_preempt_after_steps, *params_.max_preemptions));
        WHEELS_UNREACHABLE();
      } else {
        return true;  // Preempt
      }
    }

    if (MaxPreemptsReached()) {
      return false;  // Continue
    }

    // 0 - continue, 1 - preempt
    bool preempt = Either(ForkType::Preempt);

    if (preempt == 1) {
      return true;  // Preempt
    } else {
      return false;  // Continue
    }
  }

  void AddStep(Fiber* running) {
    ++steps_;

    if (params_.max_steps && (steps_ > *params_.max_steps)) {
      Prune(::fmt::format("max_steps limit = {} reached", *params_.max_steps));
    }

    if (running) {
      running->sched.f1 += 1;
    }
  }

  void ResetSteps(Fiber* next) {
    next->sched.f1 = 0;
  }

  bool ForcedPreempt(Fiber* running) const {
    size_t steps = running->sched.f1;

    return params_.force_preempt_after_steps &&
           (steps >= *params_.force_preempt_after_steps);
  }

  bool MaxPreemptsReached() const {
      return params_.max_preemptions && (preempts_ >= *params_.max_preemptions);
  }

  Fiber* PickWaiter(SortedList& wait_queue) {
    return Pick(wait_queue, ForkType::WakeOne);
  }

  Fiber* Pick(SortedList& list, ForkType type) {
    if (list.IsEmpty()) {
      return nullptr;
    }
    if (list.Size() == 1) {
      return list.Pop(0);
    }

    size_t index = Choice(list.Size(), type);
    return list.Pop(index);
  }

  bool Either(ForkType type) {
    return Choice(2, type) == 1;
  }

  size_t Choice(size_t alts, ForkType type) {
    size_t fork = fork_index_++;
    if (fork < schedule_.size()) {
      // Replay
      WHEELS_ASSERT(schedule_[fork].type == type, "Unexpected fork type");
      return schedule_[fork].index;
    } else {
      schedule_.push_back(Fork{type, alts, 0});
      return 0;  // Go left
    }
  }

  void Prune(std::string why) {
    simulator_->Prune(std::move(why));
  }

 private:
  static bool Backtrack(Schedule& s) {
    while (!s.empty()) {
      Fork& last = s.back();
      if (last.index + 1 == last.alts) {
        s.pop_back();
      } else {
        last.index++;
        return true;
      }
    }
    return false;
  }

 private:
  const Params params_;

  Schedule schedule_{};
  size_t fork_index_ = 0;

  Simulator* simulator_;

  SortedList run_queue_;
  Fiber* active_ = nullptr;

  size_t preempts_ = 0;
  size_t steps_ = 0;
};

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::fiber
