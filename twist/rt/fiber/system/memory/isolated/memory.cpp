#include "memory.hpp"

#include "static.hpp"

#include <twist/rt/fiber/user/am_i_user.hpp>
#include <twist/rt/fiber/user/syscall/random.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

//////////////////////////////////////////////////////////////////////

static const size_t kDefaultStackSize = 2 * 1024 * 1024;

//////////////////////////////////////////////////////////////////////

static heap::AllocatorParams ToHeapAllocatorParams(const Params& params) {
  return {
      .memset = params.memset_malloc
  };
}

Memory::Memory(const Params& params)
    : randomize_malloc_(params.randomize_malloc),
      segments_(StaticMemoryMapper()->MutView()),
      statics_(segments_.Static()),
      heap_(segments_.Dynamic(), ToHeapAllocatorParams(params)),
      stacks_(segments_.Dynamic(), kDefaultStackSize) {
}

bool Memory::FixedMapping() const {
  return StaticMemoryMapper()->FixedAddress();
}

void Memory::Reset() {
  heap_.HardReset();
  stacks_.HardReset();
}

void Memory::Burn() {
  segments_.Dynamic()->Reset();
  heap_.HardReset();
  stacks_.HardReset();
}

// IHeapAllocator

void* Memory::Malloc(size_t size) {
  WHEELS_ASSERT(user::AmIUser(), "User thread expected");
  void* addr = heap_.Allocate(size, HeapAllocContext());
  WHEELS_VERIFY(addr != nullptr, "Heap overflow");
  return addr;
}

void Memory::Free(void* p) {
  WHEELS_ASSERT(user::AmIUser(), "User thread expected");
  heap_.Free((char*)p);
}

bool Memory::Access(void* addr, size_t size) {
  WHEELS_ASSERT(user::AmIUser(), "User thread expected");
  return heap_.Access((char*)addr, size);
}

//

heap::AllocContext Memory::HeapAllocContext() {
  heap::AllocContext ctx;

  if constexpr (heap::Allocator::kSupportsRandomization) {
    if (randomize_malloc_) {
      ctx.random = user::syscall::RandomNumber();
    }
  }

  return ctx;
}

Stack Memory::AllocateStack(FiberId) {
  auto stack = stacks_.Allocate();
  WHEELS_VERIFY(stack.has_value(), "Stack memory overflow");
  return std::move(*stack);
}

void Memory::FreeStack(Stack stack, FiberId) {
  stacks_.Free(stack);
}

void Memory::SetMinStackSize(size_t bytes) {
  stacks_.SetMinStackSize(bytes);
}

void* Memory::AllocateStatic(size_t size) {
  void* addr = statics_.Allocate(size);
  WHEELS_VERIFY(addr != nullptr, "Static segment overflow");
  return addr;
}

void Memory::DeallocateStatics() {
  statics_.Reset();
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
