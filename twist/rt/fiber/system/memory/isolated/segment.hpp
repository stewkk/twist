#pragma once

#include <wheels/memory/view.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

using Segment = wheels::MutableMemView;

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
