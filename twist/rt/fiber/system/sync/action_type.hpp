#pragma once

namespace twist::rt::fiber {

namespace system::sync {

enum class ActionType {
  AtomicInit,
  AtomicLoad,
  AtomicDebugLoad,
  AtomicStore,
  AtomicRmwLoad,
  AtomicRmwCommit,
  AtomicDestroy,
  AtomicThreadFence,
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
