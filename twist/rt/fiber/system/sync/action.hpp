#pragma once

#include "action_type.hpp"

#include "../fiber/id.hpp"

#include <wheels/core/source_location.hpp>

// std::memory_order
#include <atomic>
#include <cstdint>

namespace twist::rt::fiber {

namespace system::sync {

struct Action {
  void* loc;  // Memory location
  ActionType type;  // AtomicLoad / AtomicStore / etc
  uint64_t value;
  std::memory_order mo;
  const char* op;  // E.g. mutex::lock or atomic::store
  wheels::SourceLocation source_loc;
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
