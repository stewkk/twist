#pragma once

#include <cstdint>
#include <cstdlib>
#include <chrono>
#include <optional>

namespace twist::rt::fiber {

namespace system {

struct Params {
  std::chrono::nanoseconds tick = std::chrono::microseconds(10);
  bool allow_detached_threads = true;  // User memory isolation mode only
  std::optional<size_t> min_stack_size;

  bool randomize_malloc = false;
  std::optional<unsigned char> memset_malloc;  // User memory isolation mode only

  bool crash_on_abort = true;

  bool forward_stdout = true;

  // User memory isolation mode only:
  bool digest_atomic_loads = false;
  // Digest atomic loads if possible
  bool allow_digest_atomic_loads = true;

  bool verbose = false;
};

}  // namespace system

}  // namespace twist::rt::fiber
