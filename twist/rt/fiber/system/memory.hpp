#pragma once

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

#include "memory/isolated/memory.hpp"

namespace twist::rt::fiber {

namespace system {

// User memory isolated from system memory
using Memory = memory::isolated::Memory;

using Stack = memory::isolated::Stack;

}  // namespace system

}  // namespace twist::rt::fiber

#else

#include "memory/shared/memory.hpp"

namespace twist::rt::fiber {

namespace system {

// Memory shared between user and simulator (kernel)
using Memory = memory::shared::Memory;

using Stack = memory::shared::Stack;

}  // namespace system

}  // namespace twist::rt::fiber

#endif
