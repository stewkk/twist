#pragma once

#include <function2/function2.hpp>

namespace twist::rt::fiber {

namespace system {

struct IFiberRoutine {
  virtual ~IFiberRoutine() = default;

  virtual void RunUser() = 0;
};

struct IFiberExitHandler {
  virtual ~IFiberExitHandler() = default;

  virtual void AtFiberExit() noexcept = 0;
};

struct IFiberUserState : IFiberRoutine, IFiberExitHandler {
};

}  // namespace system

}  // namespace twist::rt::fiber
