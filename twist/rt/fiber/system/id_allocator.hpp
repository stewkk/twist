#pragma once

#include "fiber/id.hpp"
#include "limits.hpp"

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

namespace system {

class IdAllocator {
 public:
  FiberId AllocateId() {
    WHEELS_VERIFY(next_id_ < kMaxFibers, "Too many threads");
    return ++next_id_;
  }

  static constexpr FiberId kImpossibleId = 0;

  void Free(FiberId) {
  }

  void Reset() {
    next_id_ = 0;
  }

 private:
  uint8_t next_id_ = 0;
};

}  // namespace system

}  // namespace twist::rt::fiber
