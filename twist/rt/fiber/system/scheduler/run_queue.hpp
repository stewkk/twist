#pragma once

#include "../fwd.hpp"

namespace twist::rt::fiber {

namespace system::scheduler {

struct IRunQueue {
  virtual ~IRunQueue() = default;

  virtual bool IsIdle() const = 0;
  virtual Fiber* PickNext() = 0;

  // Burn
  virtual void Remove(Fiber*) = 0;
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
