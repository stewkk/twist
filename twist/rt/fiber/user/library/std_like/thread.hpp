#pragma once

#include "atomic.hpp"
#include "chrono.hpp"

#include <twist/rt/fiber/user/syscall/spawn.hpp>
#include <twist/rt/fiber/user/syscall/futex.hpp>
#include <twist/rt/fiber/user/syscall/yield.hpp>
#include <twist/rt/fiber/user/syscall/sleep.hpp>
#include <twist/rt/fiber/user/syscall/id.hpp>

#include <wheels/core/source_location.hpp>

#include <fmt/core.h>

#include <ostream>
#include <tuple>

namespace twist::rt::fiber {

namespace user::library::std_like {

// ThreadId

struct ThreadId {
  system::FiberId fid;

  ThreadId();  // Invalid

  ThreadId(system::FiberId id)
      : fid(id) {
  }

  bool operator==(const ThreadId that) const {
    return fid == that.fid;
  }

  bool operator!=(const ThreadId that) const {
    return fid != that.fid;
  }

  bool operator<(const ThreadId that) const {
    return fid < that.fid;
  }

  bool operator>(const ThreadId that) const {
    return fid > that.fid;
  }

  bool operator<=(const ThreadId that) const {
    return fid <= that.fid;
  }

  bool operator>=(const ThreadId that) const {
    return fid >= that.fid;
  }
};

std::ostream& operator<<(std::ostream& out, ThreadId);

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber

namespace fmt {

template <>
struct formatter<twist::rt::fiber::user::library::std_like::ThreadId> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext& ctx) {
    return ctx.begin();
  }

  template <typename FmtContext>
  auto format(twist::rt::fiber::user::library::std_like::ThreadId id, FmtContext& ctx) const {
    return ::fmt::format_to(ctx.out(), "{}", id.fid);
  }
};

}  // namespace fmt

namespace std {

template<>
struct hash<::twist::rt::fiber::user::library::std_like::ThreadId> {
  std::size_t operator()(const twist::rt::fiber::user::library::std_like::ThreadId id) const noexcept {
    return hash<twist::rt::fiber::system::FiberId>{}(id.fid);
  }
};

}  // namespace std


// Thread

namespace twist::rt::fiber {

namespace user::library::std_like {

class Thread {
 public:
  using id = ThreadId;

 private:
  struct StateBase : system::IFiberUserState {
    StateBase(wheels::SourceLocation source_loc)
        : state{2, source_loc} {
    }

    Atomic<uint32_t> state;  // Rendezvous state
    ThreadId id;

    // Parent
    void Detach();
    void Join(wheels::SourceLocation call_site);

    // Child
    void AtFiberExit() noexcept override;
  };

  template <typename F>
  struct State : StateBase {
   public:
    State(wheels::SourceLocation source_loc, F&& closure)
        : StateBase(source_loc), closure_(std::move(closure)) {
    }

    void RunUser() override {
      closure_();
      // NB: unhandled exception from closure_() -> Abort in Trampoline(Fiber*)
    }

    F closure_;
  };

  // NB: In userspace!
  template <typename F, typename ... Args>
  StateBase* AllocateState(wheels::SourceLocation source_loc, F&& fn, Args&& ... args) {
    auto closure = [fn = std::move(fn), args = std::make_tuple(std::forward<Args>(args)...)]() mutable {
      std::apply(fn, std::move(args));
    };
    return new State{source_loc, std::move(closure)};
  }

 public:
  using native_handle_type = StateBase*;

 public:
  Thread();

  // https://www.cppstories.com/2021/non-terminal-variadic-args/

  template <typename F>
  Thread(F fn, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn)), source_loc) {
  }

  template <typename F, typename A>
  Thread(F fn, A&& a, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn), std::forward<A>(a)), source_loc) {
  }

  template <typename F, typename A, typename B>
  Thread(F fn, A&& a, B&& b, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn), std::forward<A>(a),
                             std::forward<B>(b)), source_loc) {
  }

  template <typename F, typename A, typename B, typename C>
  Thread(F fn, A&& a, B&& b, C&& c, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn), std::forward<A>(a),
                             std::forward<B>(b), std::forward<C>(c)), source_loc) {
  }

  template <typename F, typename A, typename B, typename C, typename D>
  Thread(F fn, A&& a, B&& b, C&& c, D&& d, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn), std::forward<A>(a),
                             std::forward<B>(b), std::forward<C>(c),
                             std::forward<D>(d)),  source_loc) {
  }

  template <typename F, typename A, typename B, typename C, typename D, typename E>
  Thread(F fn, A&& a, B&& b, C&& c, D&& d, E&& e, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Thread(AllocateState(source_loc, std::move(fn), std::forward<A>(a),
                             std::forward<B>(b), std::forward<C>(c),
                             std::forward<D>(d), std::forward<E>(e)),  source_loc) {
  }

  // More overloads?

  ~Thread();

  // Non-copyable
  Thread(const Thread&) = delete;
  Thread& operator=(const Thread&) = delete;

  // Movable
  Thread(Thread&&);
  Thread& operator=(Thread&&);

  bool joinable() const;  // NOLINT
  void join(wheels::SourceLocation call_site = wheels::SourceLocation::Current());            // NOLINT
  void detach();          // NOLINT

  id get_id() const noexcept;  // NOLINT

  static unsigned int hardware_concurrency() noexcept;  // NOLINT

  native_handle_type native_handle();

  void swap(Thread& that);

 private:
  Thread(StateBase* state, wheels::SourceLocation source_loc);

  bool HasState() const;
  StateBase* ReleaseState();

 private:
  StateBase* state_;
};

// this_thread

namespace this_thread {

inline ThreadId get_id() {  // NOLINT
  return {syscall::GetId()};
}

inline void yield() {  // NOLINT
  syscall::Yield();
}

inline void sleep_for(std::chrono::nanoseconds delay) {  // NOLINT
  syscall::SleepFor(delay);
}

inline void sleep_until(chrono::Clock::time_point deadline) {  // NOLINT
  syscall::SleepUntil(deadline);
}

}  // namespace this_thread

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
