#pragma once

#include "atomic.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>

#include <twist/rt/fiber/user/library/std_like/chrono.hpp>
#include <twist/rt/fiber/user/library/std_like/chrono/deadline.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class TimedMutex {
  enum State : uint32_t {
    Unlocked = 0,
    Locked = 1,
  };

 public:
  TimedMutex(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : locked_(State::Unlocked, source_loc) {
  }

  // Non-copyable
  TimedMutex(const TimedMutex&) = delete;
  TimedMutex& operator=(const TimedMutex&) = delete;

  // Non-movable
  TimedMutex(TimedMutex&&) = delete;
  TimedMutex& operator=(TimedMutex&&) = delete;

  // std::mutex / Lockable

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (locked_.exchange(State::Locked, std::memory_order::seq_cst, call_site) == State::Locked) {
      system::WaiterContext waiter{system::FutexType::MutexLock, "timed_mutex::lock", call_site};
      syscall::FutexWait(&locked_, State::Locked, &waiter);
    }
    owner_.Lock();
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    uint32_t unlocked = State::Unlocked;
    if (locked_.compare_exchange_weak(unlocked, State::Locked, std::memory_order::seq_cst, std::memory_order::relaxed, call_site)) {
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  bool try_lock_for(chrono::Clock::duration timeout, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    system::WaiterContext waiter{system::FutexType::MutexTryLock, "timed_mutex::try_lock_for", call_site};
    return TryLockTimed(DeadLine::FromTimeout(timeout), &waiter);
  }

  // NB: steady_clock and system_clock are both aliases of the same Clock,
  // so no template over clock type is required

  bool try_lock_until(chrono::Clock::time_point d, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    system::WaiterContext waiter{system::FutexType::MutexTryLock, "timed_mutex::try_lock_until", call_site};
    return TryLockTimed(DeadLine{d}, &waiter);
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    locked_.store(State::Unlocked, std::memory_order::seq_cst, call_site);
    syscall::FutexWake(&locked_, 1);
  }

 private:
  bool TryLockTimed(DeadLine d, system::WaiterContext* waiter) {
    while (locked_.exchange(State::Locked, std::memory_order::seq_cst, waiter->source_loc) == State::Locked) {
      if (d.Expired()) {
        return false;
      }

      syscall::FutexWait(&locked_, State::Locked, waiter);
    }
    return true;
  }

 private:
  Atomic<uint32_t> locked_;
  MutexOwner owner_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
