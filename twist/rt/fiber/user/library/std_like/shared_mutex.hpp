#pragma once

#include "atomic.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>

#include <cstdint>
// std::lock_guard, std::unique_lock
#include <mutex>
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class SharedMutex {
 public:
  SharedMutex(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : state_(Pack(false, 0), source_loc) {
  }

  // Non-copyable
  SharedMutex(const SharedMutex&) = delete;
  SharedMutex& operator=(const SharedMutex&) = delete;

  // Non-movable
  SharedMutex(SharedMutex&&) = delete;
  SharedMutex& operator=(SharedMutex&&) = delete;

  // Writer

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (true) {
      uint32_t state = state_.load(std::memory_order::seq_cst, call_site);
      auto [writer, readers] = Unpack(state);
      if (!writer && (readers == 0)) {
        if (state_.compare_exchange_strong(state, Pack(true, 0), std::memory_order::seq_cst, std::memory_order::relaxed, call_site)) {
          break;
        }
      } else {
        system::WaiterContext waiter{system::FutexType::MutexLock, "shared_mutex::lock", call_site};
        syscall::FutexWait(&state_, state, &waiter);
      }
    }
    owner_.Lock();
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    uint32_t unlocked = Pack(false, 0);
    if (state_.compare_exchange_weak(unlocked, Pack(true, 0), std::memory_order::seq_cst, std::memory_order_relaxed, call_site)) {
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    owner_.Unlock();
    state_.store(Pack(false, 0), std::memory_order::seq_cst, call_site);
    syscall::FutexWake(&state_, 0);  // All
  }

  // Reader

  void lock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (true) {
      uint32_t state = state_.load(std::memory_order::seq_cst, call_site);
      auto [writer, readers] = Unpack(state);
      if (!writer) {
        if (state_.compare_exchange_strong(state, Pack(false, readers + 1), std::memory_order::seq_cst, std::memory_order::relaxed, call_site)) {
          break;
        }
      } else {
        system::WaiterContext waiter{system::FutexType::MutexLock, "shared_mutex::lock_shared", call_site};
        syscall::FutexWait(&state_, state, &waiter);
      }
    }
  }

  bool try_lock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (true) {
      uint32_t state = state_.load(std::memory_order::seq_cst, call_site);
      auto [writer, readers] = Unpack(state);

      if (!writer) {
        if (state_.compare_exchange_strong(state, Pack(false, readers + 1), std::memory_order_seq_cst, std::memory_order::relaxed, call_site)) {
          return true;
        }
      } else {
        return false;
      }
    }
  }

  void unlock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    uint32_t old_state = state_.fetch_sub(2, std::memory_order::seq_cst, call_site);
    auto [writer, readers] = Unpack(old_state);

    ___TWIST_FIBER_USER_VERIFY(readers > 0, "Expected std::shared_mutex in readers state");

    if (readers == 1) {
      // Last reader
      syscall::FutexWake(&state_, 1);  // One
    }
  }

 private:
  static std::pair<bool, uint32_t> Unpack(uint32_t state) {
    bool writer = (state & 1) == 1;
    uint32_t readers = state >> 1;
    return {writer, readers};
  }

  static uint32_t Pack(bool writer, uint32_t readers) {
    return (readers << 1) | (writer ? 1 : 0);
  }

 private:
  Atomic<uint32_t> state_;  // writer bit + (readers << 1)
  MutexOwner owner_;  // writer
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
