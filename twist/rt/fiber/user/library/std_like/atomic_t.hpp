#pragma once

#include <twist/rt/fiber/user/library/std_like/atomic_base.hpp>

namespace twist::rt::fiber {

namespace user::library::std_like {

template <typename T>
class Atomic : public AtomicBase<T> {
  using Base = AtomicBase<T>;

 public:
  Atomic(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Base(source_loc) {
  }

  Atomic(T value, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Base(value, source_loc) {
    static_assert(sizeof(Atomic<T>) == sizeof(T));
  }

  T operator=(T new_value) {
    Base::store(new_value);
    return new_value;
  }
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
