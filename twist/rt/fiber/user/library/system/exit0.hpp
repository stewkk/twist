#pragma once

#include <twist/rt/fiber/user/syscall/abort.hpp>

namespace twist::rt::fiber {

namespace user::library::system {

[[noreturn]] inline void Exit0() {
  syscall::Abort(fiber::system::Status::Ok);
}

}  // namespace user::library::system

}  // namespace twist::rt::fiber
