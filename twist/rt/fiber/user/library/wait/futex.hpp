#pragma once

#include <twist/rt/fiber/user/library/std_like/atomic.hpp>

#include <chrono>

namespace twist::rt::fiber {

namespace user::library::futex {

struct WakeKey {
  void* loc;
};

// Wait

void Wait(std_like::Atomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst,
          wheels::SourceLocation call_site = wheels::SourceLocation::Current());

bool WaitTimed(std_like::Atomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout,
               std::memory_order mo = std::memory_order::seq_cst,
               wheels::SourceLocation call_site = wheels::SourceLocation::Current());

// Wake

WakeKey PrepareWake(std_like::Atomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace user::library::futex

}  // namespace twist::rt::fiber
