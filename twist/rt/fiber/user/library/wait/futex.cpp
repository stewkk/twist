#include "futex.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>
#include <twist/rt/fiber/user/syscall/now.hpp>

namespace twist::rt::fiber {

namespace user::library::futex {

static void* FutexKeyFor(std_like::Atomic<uint32_t>& atomic) {
  return (void*)&atomic;
}

void Wait(std_like::Atomic<uint32_t>& atomic, uint32_t old, std::memory_order mo,
          wheels::SourceLocation call_site) {
  void* key = FutexKeyFor(atomic);

  do {
    system::WaiterContext waiter{system::FutexType::Futex, "futex::Wait",
                                 call_site};
    syscall::FutexWait(key, old, &waiter);
  } while (atomic.load(mo) == old);
}

bool WaitTimed(std_like::Atomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout, std::memory_order mo,
               wheels::SourceLocation call_site) {
  void* key = FutexKeyFor(atomic);

  system::Time::Instant deadline = syscall::Now() + timeout;

  do {
    if (syscall::Now() >= deadline) {
      return false;
    }
    // Ignore errors

    system::WaiterContext waiter{system::FutexType::Futex, "futex::WaitTimed",
                              call_site};
    syscall::FutexWaitTimed(key, old, deadline, &waiter);
  } while (atomic.load(mo) == old);

  return true;
}

WakeKey PrepareWake(std_like::Atomic<uint32_t>& atomic) {
  return {FutexKeyFor(atomic)};
}

void WakeOne(WakeKey key) {
  syscall::FutexWake(key.loc, 1);
}

void WakeAll(WakeKey key) {
  syscall::FutexWake(key.loc, 0 /* 0 means all */);
}

}  // namespace user::library::futex

}  // namespace twist::rt::fiber
