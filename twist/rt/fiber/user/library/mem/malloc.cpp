#include "malloc.hpp"

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user::library::mem {

void* Malloc(size_t size) {
  return system::Simulator::Current()->UserMalloc(size);
}

void Free(void* ptr) {
  system::Simulator::Current()->UserFree(ptr);
}

}  // namespace user::library::mem

}  // namespace twist::rt::fiber

#else

namespace twist::rt::fiber {

namespace user::library::mem {

void* Malloc(size_t size) {
  return std::malloc(size);
}

void Free(void* ptr) {
  return std::free(ptr);
}

}  // namespace user::library::mem

}  // namespace twist::rt::fiber

#endif