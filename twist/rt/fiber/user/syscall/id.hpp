#pragma once

#include <twist/rt/fiber/system/fiber/id.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::FiberId GetId();

}  // namespace user::syscall

}  // namespace twist::rt::fiber
