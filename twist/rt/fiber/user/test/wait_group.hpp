#pragma once

#include <twist/rt/fiber/user/syscall/spawn.hpp>

#include <twist/rt/fiber/user/library/std_like/atomic.hpp>

#include <wheels/core/source_location.hpp>

namespace twist::rt::fiber {

namespace user::test {

class WaitGroup {
 private:
  struct UserStateNode : system::IFiberUserState {
    UserStateNode* next;
  };

  template <typename F>
  struct UserState : UserStateNode {
    explicit UserState(WaitGroup* w, F&& f)
        : wg(w), fn(std::move(f)) {
    }

    void RunUser() override {
      fn();
    }

    void AtFiberExit() noexcept override {
      wg->AtFiberExit();
      delete this;
    }

    WaitGroup* wg;
    F fn;
  };

 public:
  WaitGroup() {
  }

  template <typename F>
  WaitGroup& Add(F fn) {
    UserStateNode* r = new UserState{this, std::move(fn)};
    r->next = head_;
    head_ = r;

    ++count_;

    return *this;
  }

  template <typename F>
  WaitGroup& Add(size_t count, F fn) {
    for (size_t i = 0; i < count; ++i) {
      Add(fn);
    }

    return *this;
  }

  void Join(wheels::SourceLocation call_site = wheels::SourceLocation::Current());

 private:
  void AtFiberExit() noexcept;

 private:
  UserStateNode* head_ = nullptr;
  size_t count_{0};
  library::std_like::Atomic<uint64_t> left_{0};
  library::std_like::Atomic<uint64_t> done_{0};
};

}  // namespace user::test

}  // namespace twist::rt::fiber
