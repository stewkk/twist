#pragma once

#include "../scheduler/interrupt.hpp"

namespace twist::rt::fiber {

namespace user::assist {

inline void PreemptionPoint(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  scheduler::Interrupt(call_site);
}

}  // namespace user::assist

}  // namespace twist::rt::fiber
