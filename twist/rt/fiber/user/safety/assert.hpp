#pragma once

#include "panic.hpp"

#if !defined(NDEBUG)

#define ___TWIST_FIBER_USER_ASSERT(cond, error) \
  do {                              \
    if (!(cond)) {                  \
      twist::rt::fiber::user::Panic(twist::rt::fiber::system::SimStatus::LibraryAssert, "Assertion \"" #cond "\" failed: " #error); \
    }                               \
  } while (false)

#else

#define ___TWIST_FIBER_USER_ASSERT(cond, error) \
  do {                              \
    /* No-op */                     \
  } while (false)

#endif


#define ___TWIST_FIBER_USER_VERIFY(cond, error) \
  do {                              \
    if (!(cond)) {                  \
      twist::rt::fiber::user::Panic(twist::rt::fiber::system::Status::LibraryAssert, "Assertion \"" #cond "\" failed: " #error); \
    }                               \
  } while (false)
