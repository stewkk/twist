#pragma once

#include <twist/rt/thread/random/number.hpp>

namespace twist::rt::thread::fault {

inline uint64_t RandomUInt64() {
  return random::UInt64();
}

}  // namespace twist::rt::thread::fault
