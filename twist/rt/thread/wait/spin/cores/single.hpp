#pragma once

#include <thread>

namespace twist::rt::thread {

namespace cores::single {

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;


  void Spin() {
    std::this_thread::yield();
  }

  void operator()() {
    Spin();
  }
};

}  // namespace cores::single

}  // namespace twist::rt::thread
