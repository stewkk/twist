#pragma once

#include <twist/single_core.hpp>

#if __TWIST_SINGLE_CORE__

#include <twist/rt/thread/wait/spin/cores/single.hpp>

namespace twist::rt::thread {

using cores::single::SpinWait;

}  // namespace twist::rt::thread

#else

#include <twist/rt/thread/wait/spin/cores/multi.hpp>

namespace twist::rt::thread {

using cores::multi::SpinWait;

}  // namespace twist::rt::thread

#endif

#include <twist/rt/thread/wait/spin/relax.hpp>

namespace twist::rt::thread {

using cores::CpuRelax;

}  // namespace twist::rt::thread
