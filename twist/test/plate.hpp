#pragma once

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/safety/assert.hpp>
#include <twist/test/inject_fault.hpp>

namespace twist::test {

class Plate {
 public:
  void Access() {
    TWISTED_VERIFY(!accessed_.exchange(true, std::memory_order::relaxed),
                   "Mutual exclusion violated");

    InjectFault();

    // Non-atomic access, potential data race
    ++access_count_;

    TWISTED_VERIFY(accessed_.exchange(false, std::memory_order::relaxed),
                   "Mutual exclusion violated");
  }

  size_t AccessCount() const {
    return access_count_;
  }

 private:
  twist::ed::std::atomic<bool> accessed_{false};
  size_t access_count_{0};
};

}  // namespace twist::test
