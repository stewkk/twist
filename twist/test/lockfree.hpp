#pragma once

#include <twist/rt/fault/adversary/adversary.hpp>
#include <twist/rt/fault/adversary/lockfree.hpp>

#include <twist/ed/std_like/mutex.hpp>

namespace twist::test {

//////////////////////////////////////////////////////////////////////

inline void SetLockFreeAdversary() {
  rt::fault::SetAdversary(rt::fault::CreateLockFreeAdversary());
}

//////////////////////////////////////////////////////////////////////

template <typename T>
class ReportProgressFor {
  class Proxy {
   public:
    Proxy(T* object)
        : object_(object) {
    }

    T* operator->() {
      return object_;
    }

    ~Proxy() {
      rt::fault::Adversary()->ReportProgress();
    }

   private:
    T* object_;
  };

 public:
  template <typename... Args>
  explicit ReportProgressFor(Args&&... args)
      : object_(std::forward<Args>(args)...) {
  }

  Proxy operator->() {
    return {&object_};
  }

 private:
  T object_;
};

//////////////////////////////////////////////////////////////////////

inline void ReportProgress() {
  rt::fault::Adversary()->ReportProgress();
}

//////////////////////////////////////////////////////////////////////

struct EnablePark {
  EnablePark() {
    rt::fault::Adversary()->EnablePark();
  }

  ~EnablePark() {
    rt::fault::Adversary()->DisablePark();
  }
};

struct DisablePark {
  DisablePark() {
    rt::fault::Adversary()->DisablePark();
  }

  ~DisablePark() {
    rt::fault::Adversary()->EnablePark();
  }
};

}  // namespace twist::test
