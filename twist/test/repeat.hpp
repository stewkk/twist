#pragma once

#include <twist/test/budget.hpp>

namespace twist::test {

class Repeat {
 public:
  Repeat(TimeBudget& budget)
      : budget_(budget.MakeClient()) {
  }

  bool Test() {
    if (budget_.Test()) {
      ++iter_;
      NewIterHint();  // Feedback
      return true;
    } else {
      return false;
    }
  }

  bool operator()() {
    return Test();
  }

  size_t Iter() const {
    return iter_;
  }

  size_t IterCount() const {
    return Iter();
  }

  void Finish();

 private:
  void NewIterHint();

 private:
  twist::test::TimeBudget::Client budget_;
  size_t iter_ = 0;
};

}  // namespace twist::test
