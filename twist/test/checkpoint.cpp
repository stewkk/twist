#include <twist/test/checkpoint.hpp>

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::test {

void Checkpoint() {
  rt::fiber::system::Simulator::Current()->Checkpoint();
}

}  // namespace twist::test

#else

namespace twist::test {

void Checkpoint() {
  // No-op
}

}  // namespace twist::test

#endif
