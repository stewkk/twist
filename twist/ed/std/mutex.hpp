#pragma once

/*
 * Drop-in replacement for std::{mutex, timed_mutex}
 *
 * https://en.cppreference.com/w/cpp/thread/mutex
 * https://en.cppreference.com/w/cpp/thread/timed_mutex
 *
 * Contents:
 *   namespace twist::ed::std
 *     class mutex
 *     class timed_mutex
 */

#include <twist/rt/facade/std_like/mutex.hpp>

namespace twist::ed::std {

using rt::facade::std_like::mutex;
using rt::facade::std_like::timed_mutex;

}  // namespace twist::ed::std
