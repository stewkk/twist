#pragma once

/*
 * Drop-in replacement for std::{malloc,free}
 *
 * void* mem::Malloc(std::size_t size);
 * void mem::Free(void* ptr);
 *
 */

#include <twist/rt/facade/mem/malloc.hpp>

namespace twist::ed {

namespace mem {

using rt::facade::mem::Malloc;
using rt::facade::mem::Free;

}  // namespace mem

}  // namespace twist::ed
