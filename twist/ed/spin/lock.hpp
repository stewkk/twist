#pragma once

#include <twist/ed/std/atomic.hpp>

#include <twist/ed/spin/wait.hpp>

// std::lock_guard
#include <mutex>

namespace twist::ed {

class SpinLock {
  using ScopeGuard = ::std::lock_guard<SpinLock>;

 public:
  void Lock() {
    SpinWait spin_wait;
    while (!TryLock()) {
      while (locked_.load(::std::memory_order::relaxed)) {
        spin_wait();
      }
    }
  }

  bool TryLock() {
    return !locked_.exchange(true, ::std::memory_order::acquire);
  }

  void Unlock() {
    locked_.store(false, ::std::memory_order::release);
  }

  ScopeGuard Guard() {
    return ScopeGuard{*this};
  }

  // Lockable concept

  // NOLINTNEXTLINE
  void lock() {
    Lock();
  }

  // NOLINTNEXTLINE
  bool try_lock() {
    return TryLock();
  }

  // NOLINTNEXTLINE
  void unlock() {
    Unlock();
  }

 private:
  twist::ed::std::atomic<bool> locked_{false};
};

}  // namespace twist::ed
