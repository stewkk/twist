# Twist 🧵

[Fault injection](https://en.wikipedia.org/wiki/Fault_injection) /w [deterministic simulation](https://www.youtube.com/watch?v=4fFDFbi3toc) for modern multi-threaded C++.

## Demo
- (_entry_) [mutex_lock_order](demo/mutex_lock_order) – simple deadlock with 2 threads and 2 mutexes, exhaustive search with _DFS_ scheduler
- (_medium_) [bounded_queue](demo/bounded_queue) – bounded blocking queue, _Random_ scheduler
- (_expert_) [pthread_cond](demo/pthread_cond) – missed wake-up in `pthread_cond_wait`, _PCT_ scheduler
- and [more...](demo)


[Guide (ru)](docs/ru/guide.md)

## Simulation Features

- Determinism
  - Scheduling
  - Randomness
  - Time
  - Memory (in isolation mode) – stable addresses across runs for
    - `new` allocations,
    - thread stacks, 
    - static [thread-local] variables
- Customizable schedulers
  - _Fair_ – FIFO + time slices
  - _Coop_ – cooperative scheduler for manual simulation    
  - _Random_ – randomized time slices, run queue and wait queues, spurious wake-ups and failures, memory allocations
  - _PCT_ – [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
  - _DFS_ – exhaustive depth-first search
      - TLA<sup>+</sup>-like branching `size_t twist::ed::random::Choose(size_t alts)` and `bool twist::test::Either()`
      - `max_preemptions` bound
      - `max_steps` bound
      - non-terminating tests support (`while (true)`)
  - _Replay_
- Checks
  - Synchronization
      - Global deadlock detection
      - [Under [sequential consistency](https://jepsen.io/consistency/models/sequential) ] [simply-happens-before](https://eel.is/c++draft/intro.races#11) tracking (including `std::atomic_thread_fence`), precise data race detection (with `twist::assist::NonAtomicVar<T>` annotations)
  - Memory
    - Leaks (automatic),
    - Double-free (automatic),
    - Heap-use-after-free (with `twist::assist::MemoryAccess` or `twist::assist::Ptr<T>`)
  - Simulation digests (including atomic loads) and automatic determinism checking
- Simulator
  - Drop-in replacements for `std` synchronization primitives
  - `futex`    
  - Spurious wake-ups for futex and `condition_variable`, spurious failures for `try_lock` and `compare_exchange_weak`  
  - Static {global, local} [thread-local] variables and class members
  - `std::this_thread::sleep_{for, until}` and `std::condition_variable::wait_{for, until}` with time compression
  - `std::thread::detach` support for background threads

### Todo
- [RC11](https://plv.mpi-sws.org/scfix/paper.pdf)
- Dynamic Partial-Order Reduction

## How to use

- `#include <atomic>` → `#include <twist/ed/std/atomic.hpp>`
- `std::atomic<T>` → `twist::ed::std::atomic<T>`

### Twist-ed `SpinLock`

```cpp
#include <twist/ed/std/atomic.hpp>
#include <twist/ed/wait/spin.hpp>

class SpinLock {
 public:
  void Lock() {
    // Execution backend-aware backoff for busy waiting
    twist::ed::SpinWait spin_wait;
    
    while (locked_.exchange(true)) {  // <- Faults injected here
      spin_wait();  // ~ spin_wait.Spin();
    }
  }

  void Unlock() {
    locked_.store(false);  // <- Faults injected here
  }

 private:
  // Drop-in replacement for std::atomic<T>
  twist::ed::std::atomic<bool> locked_{false};
};
```

### Examples

- [futex](/examples/futex/main.cpp) – `twist::ed::futex::Wait`
- [spin](/examples/spin/main.cpp) – `twist::ed::SpinWait`
- [chrono](/examples/chrono/main.cpp) – `twist::ed::std::this_thread::sleep_for` / time compression
- [thread_local](/examples/thread_local/main.cpp) – static thread-local variables
- [static](/examples/static/main.cpp) – static {global, local} variables, static class members
- [debug](/examples/debug/main.cpp) – debugging

### Standard library support

[`twist/ed/std/`](/twist/ed/std), namespace `twist::ed::std::`
- `atomic`, `atomic_flag` (`atomic.hpp`)
- `mutex`, `timed_mutex` (`mutex.hpp`)
- `shared_mutex` (`shared_mutex.hpp`)  
- `condition_variable` (`condition_variable.hpp`)
- `thread` (`thread.hpp`)
- `this_thread::` (`thread.hpp`)
  - `yield`
  - `sleep_for`, `sleep_until`
  - `get_id`
- `random_device` (`random.hpp`)
- `chrono::` (`chrono.hpp`)
  - `system_clock`  
  - `steady_clock`
  - `high_resolution_clock`

### Statics

[`twist/ed/static/`](twist/ed/static) – macros for static variables

- `static T name` → `TWISTED_STATIC_VAR(T, name)`
- `static thread_local T name` → `TWISTED_STATIC_THREAD_LOCAL_VAR(T, name)`
- `static thread_local T* ptr` → `TWISTED_STATIC_THREAD_LOCAL_PTR(T, ptr)`

### Extensions

[`twist/ed/wait/`](twist/ed/wait) – waiting
- Blocking (`futex.hpp`)
  - `Wait` + `WaitTimed` / `PrepareWake` + `Wake{One,All}`
- Spinning (`spin.hpp`)
  - `SpinWait`

[`twist/ed/spin/`](twist/ed/spin) – busy waiting
- `SpinLock` (`lock.hpp`)

[`twist/ed/mutex/`](twist/ed/mutex) – mutual exclusion utilities
- `Locker<Mutex>` (`locker.hpp`)

## CMake Options

- `TWIST_ATOMIC_WAIT` – Support `{atomic, atomic_flag}::wait` (`ON` / `OFF`)

### Runtime

- `TWIST_FAULTY` – Fault injection (`ON` / `OFF`)
  - `TWIST_FAULT_PLACEMENT` – Where to inject faults: `BEFORE` (default) sync operation / `AFTER` / `BOTH` sides
- `TWIST_FIBERS` – Deterministic simulation with fibers (`ON` / `OFF`)
  - `TWIST_FIBERS_ISOLATE_USER_MEMORY` – User memory simulation (`ON` / `OFF`)
    - `TWIST_FIBERS_FIXED_USER_MEMORY` – Use fixed user memory mapping to guarantee deterministic memory addresses (`ON` / `OFF`)

## Build profiles

| Name | CMake options | Description |
| --- | --- | --- |
| _FaultyFibers_ |  `-DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_PRINT_STACKS=ON` | Fault injection + Fibers execution backend |
| _FaultyThreadsASan_ | `-DTWIST_FAULTY=ON -DASAN=ON` | Fault injection + OS threads + Address sanitizer |
| _FaultyThreadsTSan_ | `-DTWIST_FAULTY=ON -DTSAN=ON` | Fault injection + OS threads + Thread sanitizer |

## Dependencies

`binutils-dev` package required for `-DTWIST_PRINT_STACKS=ON`

## Research

### PCT
- [2016] [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)

### DPOR
- [1996] [Partial-Order Methods for the Verification of Concurrent Systems](https://patricegodefroid.github.io/public_psfiles/thesis.pdf)  
- [2005] [Dynamic Partial-Order Reduction for Model Checking Software](https://users.soe.ucsc.edu/~cormac/papers/popl05.pdf)
  - [2008] [Fair Stateless Model Checking](https://www.microsoft.com/en-us/research/publication/fair-stateless-model-checking/)
  - [2013] [Bounded Partial-Order Reduction](https://www.microsoft.com/en-us/research/publication/bounded-partial-order-reduction/)
- [2016] [A Practical Approach for Model Checking C/C++11 Code](http://plrg.eecs.uci.edu/publications/toplas16.pdf)
- [2017] [Source Sets: A Foundation for Optimal Dynamic Partial Order Reduction](https://user.it.uu.se/~parosh/publications/papers/jacm17.pdf)
  - [2017] [Comparing Source Sets and Persistent Sets for Partial Order Reduction](https://user.it.uu.se/~bengt/Papers/Full/kimfest17.pdf)
  - [2018] [Optimal Dynamic Partial Order Reduction with Observers](https://user.it.uu.se/~bengt/Papers/Full/tacas18.pdf)
- [2022] [Truly Stateless, Optimal Dynamic Partial Order Reduction](https://plv.mpi-sws.org/genmc/popl2022-trust.pdf)
  - [2023] [Reconciling Preemption Bounding with DPOR](https://people.mpi-sws.org/~viktor/papers/tacas2023-buster.pdf)
  - [2023] [Unblocking Dynamic Partial Order Reduction](https://people.mpi-sws.org/~viktor/papers/cav2023-unblocking.pdf)

### C++ Memory Model

- [2017] [Repairing Sequential Consistency in C/C++11](https://plv.mpi-sws.org/scfix/)
- [2018] [P0668R5: Revising the C++ memory model](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0668r5.html)
- [2017] [A Promising Semantics for Relaxed-Memory Concurrency](https://sf.snu.ac.kr/promise-concurrency/)
