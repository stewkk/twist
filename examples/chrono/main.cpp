#include <twist/run/cross.hpp>

#include <twist/ed/std/chrono.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/fmt/print.hpp>

#include <fmt/chrono.h>

using namespace std::chrono_literals;

auto Now() {
  return twist::ed::std::chrono::steady_clock::now();
}

int main() {
  twist::run::Cross([] {
    auto start_time = Now();

    // Time is compressed for fibers
    auto pause = twist::build::Sim() ? 1'000'000s : 1s;

    twist::ed::std::this_thread::sleep_for(pause);

    twist::ed::fmt::Println("Elapsed: {}", Now() - start_time);
  });

  return 0;
}
