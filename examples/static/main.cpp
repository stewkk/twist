#include <twist/run/cross.hpp>

#include <twist/ed/static/var.hpp>
#include <twist/ed/static/member.hpp>

#include <twist/ed/fmt/print.hpp>

#include <fmt/format.h>

using twist::ed::fmt::Println;

struct Widget {
  struct Buf {};
 public:
  Widget() {
    Println("[{}] Widget::Widget", fmt::ptr(this));

    buf_ = new Buf{};
  }

  ~Widget() {
    Println("[{}] Widget::~Widget", fmt::ptr(this));
    delete buf_;
  }

  void Foo() {
    Println("[{}] Widget::Foo", fmt::ptr(this));
  }

  void Bar() {
    Println("[{}] Widget::Bar", fmt::ptr(this));
  }

 private:
  Buf* buf_;
};

struct Gadget {
  Gadget() {
    Println("Gadget::Gadget");
  }

  ~Gadget() {
    Println("Gadget::~Gadget");
  }
};

void Baz() {
  // ~ static Widget w2;
  TWISTED_STATIC_VAR(Widget, w2);

  w2->Bar();
}

// ~ static Widget w1;
TWISTED_STATIC_VAR(Widget, w1);

// ~ static Gadget g;
TWISTED_STATIC_VAR(Gadget, g);

class Counter {
 public:
  Counter() {
    Println("Counter::Counter");
  }

  ~Counter() {
    Println("Counter::~Counter");
  }

  void Inc() {
    ++count_;
  }

  void Dec() {
    --count_;
  }

  size_t Get() {
    return count_;
  }

 private:
  size_t count_ = 0;
};

class Klass {
 public:
  Klass() {
    counter->Inc();
  }

  ~Klass() {
    counter->Dec();
  }

  static size_t Count() {
    return counter->Get();
  }

 private:
  // ~ static Counter counter;
  TWISTED_STATIC_MEMBER_DECLARE(Counter, counter);
};

// ~ Counter Klass::counter;
TWISTED_STATIC_MEMBER_DEFINE(Klass, Counter, counter);

int main() {
  twist::run::Cross([] {
    Println("Start");

    w1->Foo();

    {
      Println("-> Baz");
      Baz();
    }

    {
      Println("Klass count = {}", Klass::Count());

      {
        Klass k1;
        Println("Klass count = {}", Klass::Count());

        Klass k2;
        Println("Klass count = {}", Klass::Count());
      }

      Println("Klass count = {}", Klass::Count());
    }

    Println("Exit");
  });

  return 0;
}
