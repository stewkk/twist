#pragma once

#include "mutex.hpp"

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/wait/futex.hpp>

class SimpleCondVar {
 public:
  void Wait(Mutex& mutex) {
    uint32_t count = count_.load();
    mutex.Unlock();
    twist::ed::futex::Wait(count_, count);
    mutex.Lock();
  }

  void Signal() {
    auto wake_key = twist::ed::futex::PrepareWake(count_);
    count_.fetch_add(1);
    twist::ed::futex::WakeOne(wake_key);
  }

  void Broadcast() {
    auto wake_key = twist::ed::futex::PrepareWake(count_);
    count_.fetch_add(1);
    twist::ed::futex::WakeAll(wake_key);
  }

 private:
  // Prone to ABA
  twist::ed::std::atomic<uint32_t> count_{0};
};
