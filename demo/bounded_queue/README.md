# Run

```shell
git clone https://gitlab.com/Lipovsky/twist.git
cd twist
mkdir -p build/sim && cd build/sim
cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-14 -DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_FIBERS_ISOLATE_USER_MEMORY=ON -DTWIST_EXAMPLES=ON ../..
make twist_demo_bounded_queue
./demo/bounded_queue/twist_demo_bounded_queue
```
