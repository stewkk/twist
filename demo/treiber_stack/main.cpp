#include "treiber_stack.hpp"

#include <twist/mod/sim.hpp>

#include <twist/test/wg.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

int main() {
  twist::sim::sched::dfs::Scheduler dfs{{.max_preemptions = 5}};

  auto exp = twist::sim::Explore(dfs, [] {
    LockFreeStack<int> stack;

    twist::test::WaitGroup{}
        .Add(/*threads=*/2, [&stack] {
          stack.Push(1);
          stack.TryPop();
        })
        .Join();

  });

  fmt::println("Simulations: {}", exp.sims);

  assert(exp.found);

  {
    // Schedule, simulation result
    auto [_, result] = *exp.found;
    assert(result.status == twist::sim::Status::MemoryAccess);
    fmt::println("Stderr: {}", result.std_err);  // Corrupted memory report
  }

  return 0;
}
