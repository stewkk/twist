#include <twist/run/cross.hpp>

#include <twist/ed/std/atomic.hpp>

#include <twist/ed/fmt/print.hpp>

int main() {
  twist::run::Cross([] {
    // Your code goes here
    twist::ed::std::atomic<bool> flag{false};
    flag.store(true);

    twist::ed::fmt::Println("Hello");
  });

  return 0;
}
